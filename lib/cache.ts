// Import support for storing data in IndexedDB
// TODO: use createStore to use a named store so that users who get a dialog to request for more storage space, knows what it's for.
import { get, set, createStore } from 'idb-keyval';

/**
 * @module ElectrumCache
 * @ignore
 */

// Default to using persistent database over temporary memory storage.
let useMemory = false;

try
{
	// Attempt to get data from IDB.
	get('useIndexedData');
}
catch(error)
{
	// If unsuccessful, switch over to using memory to store data.
	useMemory = true;

	// Print a warning indicating that cache will not persist across application restarts.
	// eslint-disable-next-line no-console
	console.warn('Unable to access IndexedDB: All data caches will be stored in memory and lost when closing the application.');
}

// Initialize an empty storage in-memory.
const localMemory: Record<string, any> = {};

// Write a key-value pair into memory.
async function saveInMemory(group: string, key: string | number, value: any): Promise<void>
{
	// Initialize the storage if necessary.
	if(!localMemory[group])
	{
		localMemory[group] = new Map();
	}

	// write the key value pair to the storage.
	localMemory[group].set(key, value);
}

async function readFromMemory(group: string, key: string | number): Promise<any>
{
	// Initialize the storage if necessary.
	if(!localMemory[group])
	{
		localMemory[group] = new Map();
	}

	// write the key value pair to the storage.
	return localMemory[group].get(key);
}

const localDatabases: Record<string, any> = {};

async function saveInDatabase(group: string, key: string | number, value: any): Promise<void>
{
	// Initialize the storage if necessary.
	if(!localDatabases[group])
	{
		localDatabases[group] = createStore(group, 'data');
	}

	// write the key value pair to the storage.
	set(key, value, localDatabases[group]);
}

async function readFromDatabase(group: string, key: string | number): Promise<any>
{
	// Initialize the storage if necessary.
	if(!localDatabases[group])
	{
		localDatabases[group] = createStore(group, 'data');
	}

	// read the key value pair to the storage.
	return get(key, localDatabases[group]);
}

/**
 * Stores a key-value pair in the indexed DB.
 *
 * @param group   {string}   name of the data set the key-value pair to store belongs in.
 * @param key     {string}   name of the key identifying the data to store within the set.
 * @param value   {any}      data to store in the set, indexed by the key.
 *
 * @returns {Promise<void>}
 */
export async function storeInCache(group: string, key: string | number, value: any): Promise<void>
{
	if(useMemory)
	{
		// Pass through the request to the memory storage.
		return saveInMemory(group, key, value);
	}

	// Pass through the request to the database storage.
	return saveInDatabase(group, key, value);
}

/**
 * Gets stored data from the indexed DB.
 *
 * @param group   {string}            name of the data set the stored key-value pair belongs in.
 * @param key     {string | number}   name of the key identifying the stored data within the set.
 *
 * @returns {Promise<any>} the stored data.
 */
export async function getFromCache(group: string, key: string | number): Promise<any>
{
	if(useMemory)
	{
		return readFromMemory(group, key);
	}

	return readFromDatabase(group, key);
}
