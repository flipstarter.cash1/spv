// Import network access from electrum-cash.
import { ElectrumCluster } from '@electrum-cash/library';

// Import default servers.
import { electrumServers as defaultServers } from '@electrum-cash/servers';

// Import utility functions.
import { hasSufficientVersion } from './utilities';

// Import required type definitions.
import type { TransportScheme } from '@electrum-cash/library';
import type { ElectrumServers } from '@electrum-cash/servers';

/**
 * Export the electrum provider.
 * @ignore
 */
// eslint-disable-next-line import/no-mutable-exports
export let electrumProvider: ElectrumCluster;

/**
 * Sets up an electrum cluster, and initializes it with available servers.
 * @group Initialization
 *
 * @param application   {string}            name of the application to identify as with the servers.
 * @param transport     {TransportScheme}   which electrum transport to use when connecting with the servers.
 * @param servers       {ElectrumServers}   list of electrum backend servers to connect with.
 */
export async function initializeElectrumProvider(application: string, transport: TransportScheme = 'wss', servers: ElectrumServers = defaultServers): Promise<void>
{
	// If the electrum provider has already been initialized, do nothing.
	if(electrumProvider) return;

	// Declare the protocol version this library requires.
	const requiredVersion = '1.4.3';

	// Create a clustered connection to the bitcoin cash network.
	electrumProvider = new ElectrumCluster(application, requiredVersion);

	// Add servers to the cluster.
	for(const server of servers)
	{
		// Ignore any servers that do not support the required version.
		if(!await hasSufficientVersion(server.version, requiredVersion))
		{
			// eslint-disable-next-line no-console
			console.warn(`Skipping ${server.host} due to insufficient version (${server.version} < ${requiredVersion}).`);
			continue;
		}

		// Ignore any servers that do not support encrypted websockets.
		if(!server.transports[transport])
		{
			// eslint-disable-next-line no-console
			console.warn(`Skipping ${server.host} due to missing '${transport}' transport.`);
			continue;
		}

		// Add the server to the cluster.
		// NOTE: We don't wait for this to succeed or fail, as the following ready() call is sufficient.
		electrumProvider.addServer(server.host, server.transports[transport], transport);
	}

	// Wait for the network to be available.
	await electrumProvider.ready();
}

/**
 * Disconnects the network connection and shuts down the electrum cluster.
 * @group Initialization
 */
export async function stopElectrumProvider(): Promise<void>
{
	// If the electrum provider has not been initialized, do nothing.
	if(!electrumProvider) return;

	// Shut down the electrum provider.
	await electrumProvider.shutdown();
}
