// Import electrum provider so we can abstract out the validation of it into this file.
import { electrumProvider } from '../provider';

/**
 * Validates that the electrum provider has been initialized.
 *
 * @throws an error if the electrum provider has not yet been initialized.
 */
export async function validateElectrumProvider(functionName: string): Promise<void>
{
	// Throw an error stating that the provider must be initialized before using this call.
	if(typeof electrumProvider === 'undefined')
	{
		throw(new Error(`Before using ${functionName}(), you must call the initializeElectrumProvider() function once.`));
	}
}

/**
 * Creates a token filter according to electrum protocol requirements.
 */
export async function generateTokenFilter(includeSatoshis: boolean, includeTokens: boolean): Promise<string>
{
	// Don't include anything.
	if(!includeSatoshis && !includeTokens)
	{
		throw(new Error('Cannot create token filter that excludes both satoshis and tokens.'));
	}

	// Only include satoshis.
	if(includeSatoshis && !includeTokens)
	{
		return 'exclude_tokens';
	}

	// Only include tokens.
	if(!includeSatoshis && includeTokens)
	{
		return 'tokens_only';
	}

	// Include both satoshis and tokens.
	return 'include_tokens';
}
