/*
// Import electrum provider to use in all requests.
import { electrumProvider } from '../provider';

// Import utility functions.
import { validateElectrumProvider, generateTokenFilter } from './utilities';

// ...
import type
{
	// Primitives
	BlockHeight,
	BlockHeaderHex,
	BlockHeaderHash,

	// UTXO related requests.

}
from '../../interfaces';

/**
 * @module UTXO
 * @memberof Network
 *
 * @ignore
 */

/**
 * TODO:
 * blockchain.utxo.get_info
 */
