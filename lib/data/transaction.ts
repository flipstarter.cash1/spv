// Import cache related functions.
import { getFromCache, storeInCache } from '../cache';

// Import network related functions.
import
{
	fetchTransaction,
	fetchTransactionProof,
} from '../network';

// Import types
import type
{
	BlockHeight,
	TransactionHash,
	TransactionHex,
	TransactionGetMerkleResponse,
} from '../interfaces';

/**
 * @module ElectrumSPV
 */

/**
 * Gets a raw transaction as a hex-encoded string.
 * @group Abstractions
 *
 * @param transactionHash {TransactionHash} - hash of the transaction to use as an identifier.
 *
 * @returns {Promise<TransactionHex>} the transaction as a hex-encoded string.
 */
export async function getTransaction(transactionHash: TransactionHash): Promise<TransactionHex>
{
	// Lookup the transaction in local cache.
	const cachedTransactionHex = await getFromCache('transactions', transactionHash);

	// Return the transaction from cache.
	if(cachedTransactionHex)
	{
		return cachedTransactionHex;
	}

	// Fetch the transaction from the network.
	const fetchedTransactionHex = await fetchTransaction(transactionHash);

	// Store the fetched transaction in local cache.
	await storeInCache('transactions', transactionHash, fetchedTransactionHex);

	// Return the transaction fetched from the network.
	return fetchedTransactionHex;
}

/**
 * Gets a transaction inclusion proof.
 * @group Abstractions
 *
 * @param transactionHash {TransactionHash} - hash of the transaction to use as an identifier.
 * @param blockHeight {BlockHeight} - the height in the blockchain the transaction was included in.
 *
 * @returns {Promise<TransactionGetMerkleResponse | undefined>} the transaction inclusion proof, or undefined if it is still in the mempool.
 */
export async function getTransactionInclusionProof(transactionHash: TransactionHash, blockHeight: BlockHeight): Promise<TransactionGetMerkleResponse>
{
	// Determine a unique identity for this transaction proof to make sure it is immutable.
	// TODO: This is not correct - for it to be immutable I would need to use the hash, not the height, of the block.
	const transactionProofIdentity = `${transactionHash}:${blockHeight}`;

	// Lookup the transaction proof in local cache.
	const cachedTransactionProof = await getFromCache('transactionProofs', transactionProofIdentity);

	// Return the transaction proof from cache.
	if(cachedTransactionProof)
	{
		return cachedTransactionProof;
	}

	// Fetch the transaction proof from the network.
	const fetchedTransactionProof = await fetchTransactionProof(transactionHash, blockHeight);

	// Return undefined if no proof it available.
	if(!fetchedTransactionProof)
	{
		return undefined;
	}

	// Store the fetched transaction proof in local cache.
	await storeInCache('transactionProofs', transactionProofIdentity, fetchedTransactionProof);

	// Return the transaction proof fetched from the network.
	return fetchedTransactionProof;
}
