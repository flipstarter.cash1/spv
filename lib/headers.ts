// ...
import { hexToBin, binToHex, binToNumberUint32LE, hash256 } from '@bitauth/libauth';

// cspell:ignore uncompress

// // Import Bitcoin Cash protocol constants.
// import
// {
// 	MAX_TARGET_EXPONENT_DATA,
// 	MIN_TARGET_SIGNIFICAND,
// 	MAX_TARGET_SIGNIFICAND,
// 	MAX_COMPRESSED_DIFFICULTY_TARGET,
// 	ASERT_ACTIVATION_TIME_MTP,
// 	CW144_ACTIVATION_HEIGHT,
// } from './protocol';
//
// import type { BlockHeaderObject } from './interfaces';
//
// /** @module Blockchain **/
//
// /**
//  * Parses a hex-encoded block header string into parts.
//  *
//  * @param blockHeaderHex   {string}   block header to parse, as a hex-encoded string.
//  *
//  * @returns {Promise<BlockHeaderObject>}
//  */
// export async function parseBlockHeader(blockHeaderHex: string): Promise<BlockHeaderObject>
// {
// 	// Define the valid block header length for a hex-encoded header string.
// 	const validLength = 80 * 2;
//
// 	// Throw an error if the provided block header has an incorrect size.
// 	if(blockHeaderHex.length !== validLength)
// 	{
// 		throw(new Error(`Could not parse ${blockHeaderHex.length} byte block header, should be ${validLength} bytes.`));
// 	}
//
// 	// Extract the version number for the header.
// 	const version = parseInt("0x" + binToNumberUint32LE(hexToBin(blockHeaderHex.substring(0, 8))).toString(16), 16);
//
// 	// Extract the hash of the previous block in the chain.
// 	const previousBlockHash = blockHeaderHex.substring(8, 72);
//
// 	// Extract the merkle root of the transactions in the block.
// 	const merkleRoot = blockHeaderHex.substring(72, 136);
//
// 	// Extract the unix timestamp.
// 	// TODO: Verify if this is signed or unsigned.
// 	const timestamp = binToNumberUint32LE(hexToBin(blockHeaderHex.substring(136, 144)));
//
// 	// Extract and parse the target.
// 	const compressedTarget = hexToBin(blockHeaderHex.substring(144, 152));
//
// 	// Extract the nonce.
// 	const nonce = blockHeaderHex.substring(152, 160);
//
// 	// Return the parsed block headers.
// 	return { version, previousBlockHash, merkleRoot, timestamp, compressedTarget, nonce };
// };
//
// export async function calculateDifficultTarget(blockHeader: string): Promise<Uint8Array>
// {
// 	// Determine the block hash for the provided header.
// 	const currentBlockHash = await getBlockHashFromHeader(blockHeader);
//
// 	// Determine block height for the block header provided.
// 	const currentBlockHeight = await getBlockHeightByHash();
//
// 	// TODO: Return an initial difficulty target if no history exist before
// 	if(currentBlockHeight === 0)
// 	{
// 		return MAX_COMPRESSED_DIFFICULTY_TARGET;
// 	}
//
// 	// Parse the provided block header.
// 	const { previousBlockHash, timestamp: currentTimestamp  } = await parseBlockHeader(blockHeader);
//
// 	// Fetch the previous block to use for comparison.
// 	const previousBlockHeader = await getBlockHeaderByHash(previousBlockHash);
//
// 	// Parse the previous block header.
// 	const { timestamp: previousTimestamp } = await parseBlockHeader(previousBlockHeader);
//
// 	// Determine the block height for the previous block.
// 	const previousBlockHeight = currentBlockHeight - 1;
//
// 	// TODO: Figure out how to get the median time past.
// 	const medianTimePast = await getMedianTimePast(currentTimestamp, previousTimestamp);
//
// 	// TODO: Figure out where to store protocol constants.
// 	// TODO:
// 	if(medianTimePast >= ASERT_ACTIVATION_TIME_MTP)
// 	{
// 		// TODO: Figure out what parameters to send to this.
// 		// return calculateAsertDifficultTarget();
// 	}
//
// 	if(previousBlockHeight >= CW144_ACTIVATION_HEIGHT)
// 	{
// 		// return calculateCW44DifficultTarget();
// 	}
//
// 	// TODO: Handle EDA.
// 	// TODO: Handle Legacy.
// };
//
// // bits_to_work
// export async function deriveWorkFromCompressedTarget()
// {
// 	// return (1 << 256) // (bits_to_target(bits) + 1)
// }
//
// // target_to_bits
// export async function compressTarget(target: number): Promise<Uint8Array>
// {
// 	// Limit the target to stay below the max allowed target.
// 	const boundedTarget = Math.min(target, await uncompressTarget(MAX_COMPRESSED_DIFFICULTY_TARGET));
//
// 	//
// 	const indirectExponent = (BIT_LENGTH_OF(boundedTarget) + 7) / 8;
//
// 	if(indirectExponent <= 3)
// 	{
//
// 	}
// /*
//     mask64 = 0xffffffffffffffff
//     if size <= 3:
//         compact = (target & mask64) << (8 * (3 - size))
//     else:
//         compact = (target >> (8 * (size - 3))) & mask64
//
//     if compact & 0x00800000:
//         compact >>= 8
//         size += 1
//     assert compact == (compact & 0x007fffff)
//     assert size < 256
//     return compact | size << 24
// */
// }
//
// // bits_to_target
// export async function uncompressTarget(compressedTarget: Uint8Array): Promise<number>
// {
// 	// Split the compressed target into the indirect exponent and the significand/mantissa.
// 	// TODO: Convert these to numbers.
// 	const indirectExponent = compressedTarget.slice(0, 1);
// 	const significand = compressedTarget.slice(1);
//
// 	// Verify that the indirect exponent does not go above the maximum allowed value.
// 	if(indirectExponent > MAX_TARGET_EXPONENT_DATA)
// 	{
// 		throw(new Error(`Cannot uncompress target, provided indirect exponent (${indirectExponent}) is higher than maximum (${MAX_TARGET_EXPONENT_DATA}).`));
// 	}
//
// 	// Verify that the significand does not go below the minimum allowed value.
// 	if(significand < MIN_TARGET_SIGNIFICAND)
// 	{
// 		throw(new Error(`Cannot uncompress target, provided significand (${significand}) is lower than minimum (${MIN_TARGET_SIGNIFICAND}).`));
// 	}
//
// 	// Verify that the significand does not go above the maximum allowed value.
// 	if(significand > MAX_TARGET_SIGNIFICAND)
// 	{
// 		throw(new Error(`Cannot uncompress target, provided significand (${significand}) is higher than maximum (${MAX_TARGET_SIGNIFICAND}).`));
// 	}
//
// 	// Return the uncompressed target.
// 	if(indirectExponent < 3)
// 	{
// 		return significand >> (8 * (3 - indirectExponent))
// 	}
// 	else
// 	{
// 		return significand << (8 * (indirectExponent - 3))
// 	}
//
// }
//
// export async function getMedian(list: number[]): Promise<number>
// {
// 	// If the provided list has no numbers in it..
// 	if(list.length === 0)
// 	{
// 		// Return zero as the median.
// 		return 0;
// 	}
//
// 	// Sort the list of numbers.
// 	list.sort((a, b) => a - b);
//
// 	// Calculate the middle index of the timestamps.
// 	const middleIndex = Math.floor(list.length / 2);
//
// 	// If two entries share the middle index, return the average of the two numbers.
// 	if(middleIndex % 2)
// 	{
// 		return (list[middleIndex - 1] + list[middleIndex]) / 2;
// 	}
//
// 	// If there is only a single entry in the midpoint, return it.
// 	return list[middleIndex];
// }
//
// export async function getMedianTimePast(height: number, headers?: string): Promise<number>
// {
// 	if(!headers)
// 	{
// 		// TODO: fetch the headers.
// 	}
//
// 	// TODO: Split the chunked header list into an array of hex-encoded headers.
// 	// const headers =
//
// 	// TODO: Extract the timestamps from each header.
// 	// const timestamps = headers.map(header => )
//
// 	// Return the median value of the timestamps.
// 	return await getMedian(timestamps);
// }
//
// export async function getBlockHashFromHeader(header: string): Promise<void>
// {
//
// }
//

/**
 * TODO: Document me.
 * @group Abstractions
 */
export async function getBlockHashFromHeader(header: string): Promise<string>
{
	return binToHex(hash256(hexToBin(header)).reverse());
}

/**
 * TODO: Document me.
 * @group Abstractions
 */
export async function getMerkleRootFromHeader(header: string): Promise<string>
{
	return header.substring(72, 136);
}
