/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type { RPCBase } from '@electrum-cash/library';
import type { BlockHeight, BlockHeaderHex } from '../blockchain';

/**
 * Subscribe to receive block headers when a new block is found.
 *
 * @memberof Blockchain.Headers
 */
export type HeadersSubscribeRequest =
{
	/**  Must be: 'blockchain.headers.subscribe'. */
	method: 'blockchain.headers.subscribe';
};

/**
 * blockchain.headers.subscribe
 *
 * @memberof Blockchain.Headers
 */
export type HeadersSubscribeResponse =
{
	height: BlockHeight;
	hex: BlockHeaderHex;
};

/**
 * Notification for blockchain.headers.subscribe
 *
 * @event blockchain.headers.subscribe#notification
 * @memberof Blockchain.Headers
 */
export interface HeadersSubscribeNotification extends RPCBase
{
	method: string;
	params: [ HeadersSubscribeResponse ];
}

/**
 * Unsubscribes from receiving block headers when a new block is found.
 *
 * @memberof Blockchain.Headers
 */
export type HeadersUnsubscribeRequest =
{
	/**  Must be: 'blockchain.headers.unsubscribe'. */
	method: 'blockchain.headers.unsubscribe';
};

/**
 * blockchain.headers.unsubscribe
 *
 * @memberof Blockchain.Headers
 */
export type HeadersUnsubscribeResponse = Boolean;

/**
 * Get the latest block’s height and header.
 *
 * @memberof Blockchain.Headers
 */
export type HeadersGetTipRequest =
{
	/**  Must be: 'blockchain.headers.get_tip'. */
	method: 'blockchain.headers.get_tip';
};

/**
 * blockchain.headers.get_tip
 */
export type HeadersGetTipResponse = HeadersSubscribeResponse;
