// Import utility functions.
import { hexToBin } from '@bitauth/libauth';
import { verifyMerkleProof } from './merkle';

import type { BlockHeaderHash, BlockHeight, TransactionHash } from './interfaces';

/**
 * @module Blockchain
 * @ignore
 */

/**
 * Validates the merkle proof used to verify block inclusion in the chain defined by a trusted checkpoint.
 *
 * @ignore
 */
export async function verifyBlockInclusionProof(blockHash: BlockHeaderHash, blockHeight: BlockHeight, merkleBranches: Array<string>, merkleRootHash: string): Promise<void>
{
	// Fix block hash endianness.
	const dataHash = hexToBin(blockHash).reverse();

	// Use the block height as the data position in the set.
	const dataPosition = blockHeight;

	// Reverse endianness of the branches
	const reversedMerkleBranches = merkleBranches.map((branch) => hexToBin(branch).reverse());

	// Convert the merkle root to binary format, reversing the endianness provided.
	// NOTE: Electrum servers provide the merkle root for block chain-inclusion proofs in the opposite endianness.
	const reversedMerkleRoot = hexToBin(merkleRootHash).reverse();

	// Call verifyMerkleProof
	await verifyMerkleProof(dataHash, dataPosition, reversedMerkleBranches, reversedMerkleRoot);
}

/**
 * Validates the merkle proof used to verify transaction inclusion in a provided block.
 *
 * @ignore
 */
export async function verifyTransactionInclusionProof(transactionHash: TransactionHash, merklePosition: number, merkleBranches: Array<string>, merkleRootHash: string): Promise<void>
{
	// Fix block hash endianness.
	const dataHash = hexToBin(transactionHash).reverse();

	// Use the block height as the data position in the set.
	const dataPosition = merklePosition;

	// Reverse endianness of the branches
	const reversedMerkleBranches = merkleBranches.map((branch) => hexToBin(branch).reverse());

	// Convert the merkle root to binary format, keeping the endianness provided.
	// NOTE: Electrum servers provide the merkle root for transaction block-inclusion proofs in the expected endianness.
	const reversedMerkleRoot = hexToBin(merkleRootHash);

	// Call verifyMerkleProof
	await verifyMerkleProof(dataHash, dataPosition, reversedMerkleBranches, reversedMerkleRoot);
}
