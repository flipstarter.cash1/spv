import { hexToBin } from '@bitauth/libauth';

// TODO: Document me.
export const MAX_TARGET_EXPONENT_DATA = 0x1d;
export const MIN_TARGET_SIGNIFICAND = 0x008000;
export const MAX_TARGET_SIGNIFICAND = 0x7fffff;

// TODO: Document me.
export const MAX_COMPRESSED_DIFFICULTY_TARGET = hexToBin('1d00ffff');

// TODO: Export ASERT DAA constants.
export const ASERT_ANCHOR_HEIGHT = 1;
export const ASERT_ACTIVATION_TIME_MTP = 1;

export const CW144_ACTIVATION_HEIGHT = 1;
